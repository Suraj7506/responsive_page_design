import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MMMM YYYY',
  },
  display: {
    dateInput: 'MMMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-invoice-data',
  templateUrl: './invoice-data.component.html',
  styleUrls: ['./invoice-data.component.css'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class InvoiceDataComponent implements OnInit {
  branches: string[] = [
    'Andhra Pradesh',
    'Arunachal Pradesh',
    'Assam',
    'Bihar',
    'Chhattisgarh',
    'Goa',
    'Gujarat',
    'Haryana',
    'Himachal Pradesh',
    'Jharkhand',
    'Karnataka',
    'Kerala',
    'Madhya Pradesh',
    'Maharashtra',
    'Manipur',
    'Meghalaya',
    'Mizoram',
    'Nagaland',
    'Odisha',
    'Punjab',
    'Rajasthan',
    'Sikkim',
    'Tamil Nadu',
    'Telangana',
    'Tripura',
    'Uttar Pradesh',
    'Uttarakhand',
    'West Bengal'
  ];

  selectedBranch: string = '';
  datePipe: DatePipe = new DatePipe('en-US');


  minDate: Date;
  maxDate: Date;
  formattedDate!: string | null;
  cardList:any[] = [];
  orderList:any[] = [];
  selectedDate!: Date | null;
  
  constructor() {
    const today = new Date();
    this.minDate = new Date(today.getFullYear() - 5, today.getMonth(), today.getDate());
    this.maxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
  }
  ngOnInit(): void {
    this.selectedDate = new Date()
    this.cardList = [ 
      {title:'View Funds',description:'You have used this service 20 times in last one week!', btnName: 'Action CTA >>'},
      {title:'Manage Users',description:'You have used this service 20 times in last one week!', btnName: 'Action CTA >>'},
      {title:'View Orders',description:'You have used this service 20 times in last one week!', btnName: 'Action CTA >>'},
    ]

    this.orderList = [ 
      {title:'18',description:'Total Orders', icon:'inventory_2'},
      {title:'10',description:'In Progress', icon:'schedule'},
      {title:'5',description:'Completed Order', icon:'move_to_inbox'},
      {title:'3',description:'Cancel Order', icon:'free_cancellation'},
    ]
  }

  formatSelectedDate(selectedDate: Date): void {
    this.formattedDate = selectedDate ? new DatePipe('en-US').transform(selectedDate, 'MMMM yyyy') : null;
    // Update the selected date value of the mat-datepicker input
    this.selectedDate = selectedDate;
  }

}