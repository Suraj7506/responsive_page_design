import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MatMenuTrigger } from '@angular/material/menu';
import { PopupComponent } from '../popup/popup.component';
import { MatDialog } from '@angular/material/dialog';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  @ViewChild('sidenav') sidenav!: MatSidenav;
  @ViewChild('servicesMenu') servicesMenu!: MatMenuTrigger;
  @Output('menuToggle') menuToggle = new EventEmitter<boolean>();
  @Output('menuEndToggle') menuEndToggle = new EventEmitter<boolean>();
  showMenu = false;
  showPopup = false;
  isMobileView = false;
  showDrawerButtons = false;
  notificationCount = 2;

  constructor(private dialog: MatDialog,private breakpointObserver: BreakpointObserver) {

    this.breakpointObserver.observe([Breakpoints.Handset]).subscribe((result) => {
      this.isMobileView = result.matches;
    });
  }

  openPopup() {
    this.dialog.open(PopupComponent);
  }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  toggleDashboardMenu(){
    console.log('test')
    this.menuToggle.emit(true)
  }

  toggleSidebarMenu(){
    this.showDrawerButtons = !this.showDrawerButtons;
    this.menuEndToggle.emit(); 
   } 

}
